FROM registry.gitlab.com/lipoqil/gpx-scraper/ci-base

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1 && bundle config set --local deployment true

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

CMD bundle exec rspec
