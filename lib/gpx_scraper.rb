# frozen_string_literal: true

require 'bundler/setup'
require 'nokogiri'

# Getting GPS coordinates from HTML page into a GPX file
class GPXScraper
  COORDINATES_PATTERN = /(?<latitude>\d{2}.\d+)N, ?(?<longitude>\d{2}.\d+)E/i

  # @param [String] gpx_output_path Path at which GPX file will be saved
  # @param [String] page_address HTML page to be scraped
  #   collection
  def initialize(gpx_output_path:, page_address:)
    @gpx_output_path = gpx_output_path
    @page_address = page_address
    @html_document = Nokogiri::HTML File.read(@page_address)
  end

  def scrape
    gpx_view = ERB.new(File.read('lib/gpx_scraper/gpx.xml.erb'))

    File.write(@gpx_output_path, gpx_view.result(binding))
  end

  # @return [Array<Hash{Symbol->String (frozen)}>]
  def waypoints
    @html_document.xpath('//tr').each_with_object([]) do |table_row, waypoints|
      coordinates = table_row.text.match(COORDINATES_PATTERN)

      next unless coordinates

      name = table_row.xpath('td[1]').text
      waypoints << { name:, latitude: coordinates[:latitude], longitude: coordinates[:longitude] }
    end
  end

  def gpx_name
    @html_document.title || File.basename(@page_address, '.*')
  end
end
