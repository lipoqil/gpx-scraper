# frozen_string_literal: true

describe GPXScraper do
  let(:scraper) { GPXScraper.new(page_address:, gpx_output_path: 'tmp/test.gpx') }

  describe '#scrape' do
    let(:page_address) { 'spec/fixtures/Divoká Šárka: Oranžový okruh (V3-V4) - VARP.html' }

    it 'generates valid GPX' do
      scraper.scrape

      expect(system('SAXCount tmp/test.gpx')).to be_a TrueClass
    end
  end

  describe '#gpx_name' do
    subject { scraper.gpx_name }

    context 'for page with title' do
      let(:page_address) { 'spec/fixtures/Divoká Šárka: Oranžový okruh (V3-V4) - VARP.html' }

      it { is_expected.to eq 'Divoká Šárka: Oranžový okruh (V3-V4) - VARP' }
    end

    context 'for page without' do
      let(:page_address) { 'spec/fixtures/Divoká Šárka: Oranžový okruh (V3-V4) - VARP - without title.html' }

      it { is_expected.to eq 'Divoká Šárka: Oranžový okruh (V3-V4) - VARP - without title' }
    end
  end
end
