default:

build-ci-base:
	docker build --no-cache -f spec/Dockerfile.ci-base -t registry.gitlab.com/lipoqil/gpx-scraper/ci-base spec

push-ci-base:
	docker push registry.gitlab.com/lipoqil/gpx-scraper/ci-base

build-ci-base-and-push: build-ci-base push-ci-base

local-ci-test: build-ci-base
	docker build --no-cache -t registry.gitlab.com/lipoqil/gpx-scraper:passed-tests .
	docker run registry.gitlab.com/lipoqil/gpx-scraper:passed-tests

