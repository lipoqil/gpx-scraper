# GPX scraper

Tool for getting GPS coordinates from HTML page into a GPX file.

## Motivation

I bought physical guide for climbing locations, but I wanted to navigate to 
those place via my GPS watch or at least a phone. Authors of the guide made 
pretty good job providing each place with QR codes to map services where you 
could find the place, but it was still kinda cumbersome to do this for each 
place separately.

## Technical

### Docker

Specs are using xerces to validate output XML / GPX, which is a system 
dependency. To make this work on (Gitlab) CI, there are two Dockerfiles
- _spec/Dockerfile.ci-base_ &mdash; Creates environment / image for CI build 
  runtime to be based on
  - to build the image run `make build-ci-base`
  - to push the image run `make push-ci-base`
  - to do both at once run `make build-ci-base-and-push`
- _Dockerfile_ &mdash; Serves as a testing environment for the previous image
  - to test the first image run `make local-ci-test`
